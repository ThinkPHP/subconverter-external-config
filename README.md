# Subconverter External Config



## 使用

把 `https://gitlab.com/ThinkPHP/subconverter-external-config/-/raw/main/standard.ini` 添加到 [sub-web](https://github.com/CareyWang/sub-web) 的 `远程配置` 里即可。

或参照 [此文档](https://github.com/tindy2013/subconverter/blob/master/README-cn.md#%E5%A4%96%E9%83%A8%E9%85%8D%E7%BD%AE) 把 `https%3A%2F%2Fgitlab.com%2FThinkPHP%2Fsubconverter-external-config%2F-%2Fraw%2Fmain%2Fstandard.ini` 添加到 [subconverter](https://github.com/tindy2013/subconverter/) 的 `URL` 里。
